# Test Trigger

During the merge window, running all tests for every git-push there is might
prove to be too heavy a toll on our LAVA resources.

Instead, run only the sanity tests and leave the full battery of tests to run
upon requuest, via an API call from this project on a schedule that we can
control.

## Usage

1. Set the GITLAB_PROJECT_ID in .gitlab-ci.yml. This ID can be the project name
or the project's numeric ID found at the homepage of a gitlab repository, right
below its name.

2. Set GITLAB_API_TOKEN to a secret token value of a user who has access API
access to the mirror repository.

3. Go to CI/CD -> Schedules and create a schedule to run every 6h ("0 */6 * * *").
