#!/bin/bash

set -e

CURL="curl --silent --fail"
project_id="${GITLAB_PROJECT_ID//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"
echo "API URL: ${project_url}"

# Get latest pipeline
pipeline_id="$(${CURL} "${project_url}/pipelines" | jq -r .[0].id)"
echo "Latest pipeline ID: ${pipeline_id}"

# ID's of manual jobs
${CURL} "${project_url}/pipelines/${pipeline_id}/jobs?per_page=100&scope[]=manual" | jq '.[] | select(.stage=="test").id' | while read -r job; do
    # Start manual job
    echo
    echo "* Triggering job ${job}..."
    ${CURL} --request POST --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" "${project_url}/jobs/${job}/play" | jq -r '.status'
done

echo "Done"
